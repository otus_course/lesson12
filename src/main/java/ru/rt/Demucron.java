package ru.rt;

import java.util.Arrays;

public class Demucron {
    int [][] table;
    int [] sum;
    boolean [] used;
    boolean [] current;
    boolean next = true;
    int [][] result = new int[0][0];

    public Demucron(int [][] table){
        this.table = table;
        this.sum = new int[table.length];
        this.used = new boolean[table.length];
        this.current = new boolean[table.length];

        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                sum[j] += table[i][j];
            }
        }
    }

    public int [][] calc(){
        while (next){
            calcLevel();
            diff();
        }


        return result;
    }

    private void diff() {
        for (int i = 0; i < table.length; i++) {
            if(current[i]){
                for (int j = 0; j < sum.length; j++) {
                    sum[j] -= table[i][j];
                }
            }
        }
    }

    private void calcLevel() {
        Arrays.fill(current,false);
        next = false;
        for (int i = 0; i < sum.length; i++) {
            if(sum[i] == 0 && !used[i]){
                addLevel();
                next = true;
                used[i] = true;
                current[i] = true;
                addElement(i);
            }
        }
    }

    private void addLevel(){
        if(next){
            return;
        }
        int [][] newresult = new int[result.length+1][];
        for (int i = 0; i < result.length; i++) {
            newresult[i] = result[i];
        }
        result = newresult;
    }

    private void addElement( int element){
        int[] lvl = result[result.length-1];
        int[] newlvl;
        if(lvl == null){
            newlvl = new int[1];
        }else{
            newlvl = new int[lvl.length+1];
        }
        for (int i = 0; i < newlvl.length-1; i++) {
            newlvl[i] = lvl[i];
        }
        newlvl[newlvl.length-1] = element;
        result[result.length-1] = newlvl;
    }
}
